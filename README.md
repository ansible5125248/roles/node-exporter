# Node-exporter
Role for installing node-exporter metric service.

## Requirements
```
ansible > 2.7
```

```
pip install jmepath
```

## Role Tags
|Tag|Description|
|-|-|
|node_exporter_prerequisites|Create group and users|
|node_exporter_install|Collect required information like URLs and install node-exporter|
|node_exporter_smatrmon|Enable smartmon script for nvme data collecting|
|node_exporter_config|Add hased password and config systemd.service file|

## Role Variables
|Variable|Default|Description|Multivalue|Required|
|-|-|-|-|-|
|node_exporter_user|node-exporter|User for node-exporter service|No|No|
|node_exporter_pw|-|Password for web authentication|No|No|
|node_exporter_workdir|"/data/services/node-exporter"|Working directory for service|No|No|
|version|"v1.3.1"|Version of node-exporter|No|No|
|is_nvme|false|Enable or disable smartmon script **true** or **false**|No|No|

## Dependencies
> No
## TODO
